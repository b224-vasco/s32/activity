let http = require("http");

let port = 4000;

http.createServer((request, response) =>{

	// The HTTP method of the incoming request can be accessed via the method property of "request" parameter.
	// The method "GET" means that we will be retrieving or reading information
	if(request.url == "/" && request.method=="GET"){
		// Requests the "/items" path and "GETS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// ends the response process
		response.end('Welcome to Booking System')
	}

	// The method "POST" means that we will be adding or creating information
	if(request.url == "/profile" && request.method == "GET"){
		// Requests the "/addItems" path and "SENDS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile!');
	}

	if(request.url == "/courses" && request.method == "GET"){
		// Requests the "/addItems" path and "SENDS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Here's our courses available`);
	}

	if(request.url == "/addcourse" && request.method == "POST"){
		// Requests the "/addItems" path and "SENDS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Add a course to our resources");
	}

	if(request.url == "/updatecourse" && request.method == "PUT"){
		// Requests the "/addItems" path and "SENDS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Update a course to our resources");
	}

	if(request.url == "/archivecourses" && request.method == "DELETE"){
		// Requests the "/addItems" path and "SENDS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}


}).listen(port);

console.log(`Server is running at localhost: ${port}`);